package edu.ifpb.dac.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.PostPersist;

/**
 *
 * @author Ricardo Job
 */
public class Auditoria {

    @PostPersist
    public void executaDepoisPersist(Object object) {
        BufferedWriter saida = null;
        String usuario = "Chiquinha";

        try {
            StringBuilder valor = new StringBuilder();
            File file = new File("texto.txt");
            if (file.exists()) {
                lerArquivo(file, valor);
            }
            escreverArquivo(valor, usuario, object);
            saida = new BufferedWriter(new PrintWriter(file));
            saida.append(valor);
            saida.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void escreverArquivo(StringBuilder valor, String usuario, Object object) {
        valor.append("Data: ").append(new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss").format(new Date()));
        valor.append("\tUsuario: ").append(usuario);
        valor.append("\tExecutando o persist da Classe: ").append(object.getClass().getSimpleName()).append("\t Valor: ").append(object);
        valor.append("\n");
    }

    private void lerArquivo(File file, StringBuilder valor) throws FileNotFoundException, IOException {
        try (BufferedReader leitor = new BufferedReader(new FileReader(file))) {
            String eachLine = leitor.readLine();
            while (eachLine != null) {
                valor.append(eachLine);
                valor.append("\n");
                eachLine = leitor.readLine();
            }
        }
    }
}
