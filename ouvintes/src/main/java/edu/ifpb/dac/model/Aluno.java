package edu.ifpb.dac.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PostPersist;

/**
 *
 * @author Ricardo Job
 */
@Entity
@EntityListeners({Auditoria.class})
public class Aluno implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private String nome;

    public Aluno() {
    }

    public Aluno(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return this.nome;
    }
//    @PostPersist
//    public void executaDepoisPersiste() {
//        System.out.println("Executando o this: " + this);
//    }
}
