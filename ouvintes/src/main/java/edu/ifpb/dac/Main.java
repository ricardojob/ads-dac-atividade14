package edu.ifpb.dac;

import edu.ifpb.dac.infra.FacadePessoa;

/**
 *
 * @author Ricardo Job
 */
public class Main {

    public static void main(String[] args) {
        FacadePessoa facade = new FacadePessoa();
        facade.gravaPessoa();
    }
}
