package edu.ifpb.dac.infra;

import edu.ifpb.dac.model.Aluno;
import edu.ifpb.dac.model.Pessoa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Ricardo Job
 */
public class PessoaDAO {

    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("ouvintes");
    private EntityManager em = emf.createEntityManager();

    public synchronized boolean salvar(Object object) {

        em.getTransaction().begin();
        try {
            //PrePesist
            em.persist(object);
            //PostPesist
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        return false;
    }

    public void imprime() {
        List<Pessoa> pessoas = em.createQuery("Select p From Pessoa p").getResultList();
        System.out.println("----------Listando as Pessoa-------");

        for (Pessoa pessoa : pessoas) {
            System.out.println(pessoa);
        }

        List<Aluno> alunos = em.createQuery("Select p From Aluno p").getResultList();
        System.out.println("----------Listando os Alunos-------");
        for (Aluno aluno : alunos) {
            System.out.println(aluno);
        }
    }
}
