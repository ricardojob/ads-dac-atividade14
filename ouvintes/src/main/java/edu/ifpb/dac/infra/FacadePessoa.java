
package edu.ifpb.dac.infra;

import edu.ifpb.dac.model.Aluno;
import edu.ifpb.dac.model.Pessoa;

/**
 *
 * @author Ricardo Job
 */


public class FacadePessoa {

    PessoaDAO dao = new PessoaDAO();

    public void gravaPessoa() {

        Pessoa pessoa = new Pessoa("Chaves!!");
        Aluno aluno = new Aluno("Kiko");
        dao.salvar(pessoa);
        dao.salvar(aluno);
        dao.imprime();
    }
}
