package edu.ifpb.dac.faces;

import edu.ifpb.dac.service.NotificaEJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

/**
 *
 * @author Ricardo Job
 */
@Named(value = "controlador")
@SessionScoped
public class Controlador implements Serializable {

    private String mensagem;
    private String mensagemResposta;

    @EJB
    private NotificaEJB servico;

    public Controlador() {
    }

    public String enviar() {
        mensagemResposta = servico.mensagem(mensagem);
        mensagem = "";
        return null;
    }

    public String getMensagem() {
        return mensagem;
    }

    public String getMensagemResposta() {
        return mensagemResposta;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public void setMensagemResposta(String mensagemResposta) {
        this.mensagemResposta = mensagemResposta;
    }

}
