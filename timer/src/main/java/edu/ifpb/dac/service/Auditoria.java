package edu.ifpb.dac.service;

import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

/**
 *
 * @author Ricardo Job
 */
public class Auditoria {

    @AroundInvoke
    public Object interceptador(InvocationContext ic) throws Exception {
        System.out.println("Antes da execução do método");
        //  Invocando o método e capturando seu retorno
        Object retornoDoMetodoDeNegocio = ic.proceed();
        System.out.println("Depois da execução do método");
        return retornoDoMetodoDeNegocio;
    }
}
