package edu.ifpb.dac.service;

import javax.ejb.Stateless;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptors;
import javax.interceptor.InvocationContext;

/**
 *
 * @author Ricardo Job
 */
@Stateless
//A nível de classe
//@Interceptors({Auditoria.class})
public class NotificaEJB {

    //A nível de método
    @Interceptors({Auditoria.class})
    public String mensagem(String nome) {
        return "Olá, " + nome;
    }

    // Interceptador Interno
    @AroundInvoke
    public Object interceptador(InvocationContext ic) throws Exception {
        System.out.println("Antes da execução do método - interceptador interno");
        //  Invocando o método e capturando seu retorno
        Object retornoDoMetodoDeNegocio = ic.proceed();
        System.out.println("Depois da execução do método - interceptador interno");
        return retornoDoMetodoDeNegocio;
    }
}
